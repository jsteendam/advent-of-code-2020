fn solve1(filename: &String) -> i64 {
    let content: Vec<String> = tools::file_to_lines(filename);
    let start_time: i64 = content[0].parse().expect("Invalid content!");
    let busses: Vec<i64> = content[1]
        .split(",")
        .filter(|part| part.parse::<i64>().is_ok())
        .map(|part| part.parse().expect("Invalid content!"))
        .collect();
    let mut earliest_bus = 0i64;
    let mut earliest_departure = i64::MAX;
    for bus in busses {
        let bus_round_number = (start_time / bus) + 1;
        let next_departure = bus * bus_round_number;
        if next_departure < earliest_departure {
            earliest_bus = bus;
            earliest_departure = next_departure;
        }
    }
    return (earliest_departure - start_time) * earliest_bus;
}

// Modulo helper that returns a positive remainder
fn modulo(a: i128, n: i128) -> i128 {
    let r = a % n;
    return if r < 0 { r + n } else { r };
}

struct DepartureConstraint {
    delta_time: i128,
    bus_id: i128,
}

impl DepartureConstraint {
    fn a(&self) -> i128 {
        modulo(self.bus_id - self.delta_time, self.bus_id)
    }
    fn n(&self) -> i128 {
        self.bus_id
    }
    fn validate(&self, value: i128) -> bool {
        (value % self.bus_id) == self.a()
    }
}

fn solve2_algebraic(input: &String) -> i128 {
    let mut constraints = Vec::new();
    let mut delta = 0;
    for bus in input.split(",") {
        if let "x" = bus {
        } else {
            constraints.push(DepartureConstraint {
                delta_time: delta,
                bus_id: bus.parse().expect("Not a number!"),
            })
        };
        delta += 1;
    }

    // Simplify: ax + y ≡ b (mod n)
    //       to:      x ≡ b (mod n)
    //   return: (n, b)
    let solve_part = |mut a: i128, mut y: i128, mut b: i128, n: i128| {
        if y > 0 {
            b = modulo(b - y, n);
            y = 0
        }
        for i in 0..n {
            if (a * i) % n == 1 {
                a = 1;
                b = (b * i) % n;
                break;
            }
        }
        assert_eq!(a, 1);
        assert_eq!(y, 0);
        (n, b)
    };

    // Simplify: a1*(a2x + y2) + y1
    //       to: ax + y
    //   return: (a, y)
    //
    // Arguments:
    //   equation: (a1, y1)
    //       part: (a2, y2)
    let simplify_equation = |equation: (i128, i128), part: (i128, i128)| {
        let a1 = equation.0;
        let y1 = equation.1;
        let a2 = part.0;
        let y2 = part.1;
        let a = a1 * a2;
        let y = a1 * y2 + y1;
        (a, y)
    };

    let a1 = constraints[0].a();
    let n1 = constraints[0].n();
    let mut equation = (n1, a1);
    for constraint in constraints.iter().skip(1) {
        let ak = constraint.a();
        let nk = constraint.n();
        let result = solve_part(equation.0, equation.1, ak, nk);
        equation = simplify_equation(equation, result);
    }

    return equation.1;
}

fn solve2_lcd(input: &String) -> i128 {
    let mut constraints = Vec::new();
    let mut delta = 0;
    for bus in input.split(",") {
        if let "x" = bus {
        } else {
            constraints.push(DepartureConstraint {
                delta_time: delta,
                bus_id: bus.parse().expect("Not a number!"),
            })
        };
        delta += 1;
    }
    for constraint in &constraints {
        println!("t ≡ {} (mod {})", constraint.a(), constraint.n());
    }

    let mut t = 0;
    let mut step_size = 1;
    let mut constraint_id = 0;
    loop {
        if constraints[constraint_id].validate(t) {
            step_size = num::integer::lcm(step_size, constraints[constraint_id].bus_id);
            constraint_id += 1;
            println!(
                "Found bus_id={:?} t={:?} step_size={:?} !",
                constraints[constraint_id - 1].bus_id,
                t,
                step_size
            );

            if constraint_id == constraints.len() {
                break;
            }
        }
        t += step_size;
    }

    return t;
}

fn solve2(input: &String) -> i128 {
    solve2_algebraic(input)
    // solve2_lcd(input)
}

#[cfg(test)]
mod tests {
    use crate::{solve1, solve2};
    #[test]
    fn solution1() {
        assert_eq!(solve1(&"day13/src/test_input.txt".to_string()), 295);
    }
    #[test]
    fn solution2() {
        assert_eq!(
            solve2(&tools::file_to_lines("day13/src/test_input.txt")[1]),
            1068781
        );
        assert_eq!(solve2(&"17,x,13,19".to_string()), 3417);
        assert_eq!(solve2(&"67,7,59,61".to_string()), 754018);
        assert_eq!(solve2(&"67,x,7,59,61".to_string()), 779210);
        assert_eq!(solve2(&"67,7,x,59,61".to_string()), 1261476);
        assert_eq!(solve2(&"1789,37,47,1889".to_string()), 1202161486);
    }
}

fn main() {
    println!("What is the ID of the earliest bus you can take to the airport multiplied by the number of minutes you'll need to wait for that bus?");
    tools::time_and_print_solution(solve1, &"day13/src/input.txt".to_string());

    println!("What is the earliest timestamp such that all of the listed bus IDs depart at offsets matching their positions in the list?");
    tools::time_and_print_solution(solve2, &tools::file_to_lines("day13/src/input.txt")[1]);
}
