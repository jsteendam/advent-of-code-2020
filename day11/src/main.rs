use std::io::BufRead;
use std::{fs::File, io::BufReader};

#[derive(Clone, PartialEq, Copy)]
enum PlaceType {
    Floor,
    EmptySeat,
    OccupiedSeat,
}

impl From<char> for PlaceType {
    fn from(c: char) -> Self {
        match c {
            '.' => PlaceType::Floor,
            'L' => PlaceType::EmptySeat,
            '#' => PlaceType::OccupiedSeat,
            _ => unreachable!(),
        }
    }
}

#[derive(Clone, PartialEq)]
struct WaitingArea {
    places: Vec<Vec<PlaceType>>,
}

impl WaitingArea {
    fn width(&self) -> i64 {
        match self.places.first() {
            Some(row) => row.len() as i64,
            None => 0,
        }
    }
    fn height(&self) -> i64 {
        self.places.len() as i64
    }
    fn contains(&self, x: i64, y: i64) -> bool {
        x >= 0 && x < self.width() && y >= 0 && y < self.height()
    }
    fn at(&self, x: i64, y: i64) -> PlaceType {
        self.places[y as usize][x as usize]
    }
    fn set(&mut self, x: i64, y: i64, value: PlaceType) {
        self.places[y as usize][x as usize] = value;
    }
}

impl<T> From<T> for WaitingArea where T: BufRead {
    fn from(reader: T) -> Self {
        WaitingArea {
            places: reader
                .lines()
                .map(|line| line.unwrap().chars().map(|c| PlaceType::from(c)).collect())
                .collect(),
        }
    }
}

fn read_file(filename: &str) -> WaitingArea {
    let file = File::open(filename).expect("Can't open file!");
    WaitingArea::from(BufReader::new(file))
}

fn count_occupied_adjacent_seats(x: i64, y: i64, area: &WaitingArea) -> i64 {
    let mut occupied_seats = 0;
    for i in -1..=1 {
        for j in -1..=1 {
            let check_x = x + i;
            let check_y = y + j;
            if (check_x == x && check_y == y) || !area.contains(check_x, check_y) {
                continue;
            }
            if area.at(check_x, check_y) == PlaceType::OccupiedSeat {
                occupied_seats += 1;
            }
        }
    }
    return occupied_seats;
}

fn count_occupied_seats(x: i64, y: i64, area: &WaitingArea) -> i64 {
    let check_line = |step_x: i64, step_y: i64| -> bool {
        if step_x == 0 && step_y == 0 {
            return false;
        }
        let mut check_x = x;
        let mut check_y = y;
        loop {
            check_x += step_x;
            check_y += step_y;
            if !area.contains(check_x, check_y) {
                return false;
            }
            match area.at(check_x, check_y) {
                PlaceType::Floor => {}
                PlaceType::EmptySeat => return false,
                PlaceType::OccupiedSeat => return true,
            }
        }
    };

    let mut occupied_seats = 0;
    for i in -1..=1 {
        for j in -1..=1 {
            if check_line(i, j) {
                occupied_seats += 1;
            }
        }
    }
    return occupied_seats;
}

fn next_iteration(area: &WaitingArea) -> WaitingArea {
    let mut next = area.clone();
    for y in 0..area.height() {
        for x in 0..area.width() {
            let next_place = match area.at(x, y) {
                PlaceType::Floor => PlaceType::Floor,
                PlaceType::EmptySeat => {
                    if count_occupied_adjacent_seats(x, y, area) == 0 {
                        PlaceType::OccupiedSeat
                    } else {
                        PlaceType::EmptySeat
                    }
                }
                PlaceType::OccupiedSeat => {
                    if count_occupied_adjacent_seats(x, y, area) >= 4 {
                        PlaceType::EmptySeat
                    } else {
                        PlaceType::OccupiedSeat
                    }
                }
            };
            next.set(x, y, next_place);
        }
    }
    return next;
}

fn next_iteration2(area: &WaitingArea) -> WaitingArea {
    let mut next = area.clone();
    for y in 0..area.height() {
        for x in 0..area.width() {
            let next_place = match area.at(x, y) {
                PlaceType::Floor => PlaceType::Floor,
                PlaceType::EmptySeat => {
                    if count_occupied_seats(x, y, area) == 0 {
                        PlaceType::OccupiedSeat
                    } else {
                        PlaceType::EmptySeat
                    }
                }
                PlaceType::OccupiedSeat => {
                    if count_occupied_seats(x, y, area) >= 5 {
                        PlaceType::EmptySeat
                    } else {
                        PlaceType::OccupiedSeat
                    }
                }
            };
            next.set(x, y, next_place);
        }
    }
    return next;
}

fn solve(first_area: &WaitingArea, get_next_iteration: impl Fn(&WaitingArea) -> WaitingArea) -> usize {
    let mut area = first_area.clone();
    loop {
        let next_area = get_next_iteration(&area);
        if next_area == area {
            area = next_area;
            break;
        } else {
            area = next_area;
        }
    }
    let mut occupied_count = 0;
    for y in 0..area.height() {
        for x in 0..area.width() {
            if area.at(x, y) == PlaceType::OccupiedSeat {
                occupied_count += 1;
            }
        }
    }
    return occupied_count;
}

fn solve1(first_area: &WaitingArea) -> usize {
    solve(&first_area, next_iteration)
}

fn solve2(first_area: &WaitingArea) -> usize {
    solve(&first_area, next_iteration2)
}

#[cfg(test)]
mod tests {
    use crate::{read_file, solve1, solve2};

    #[test]
    fn solution1() {
        let test_input = read_file("day11/src/test_input.txt");
        assert_eq!(solve1(&test_input), 37);
    }

    #[test]
    fn solution2() {
        let test_input = read_file("day11/src/test_input.txt");
        assert_eq!(solve2(&test_input), 26);
    }
}

fn main() {
    let input = read_file("day11/src/input.txt");
    println!("How many seats end up occupied?");
    tools::time_and_print_solution(solve1, &input);
    println!("Given the new visibility method and the rule change for occupied seats becoming empty, once equilibrium is reached, how many seats end up occupied?");
    tools::time_and_print_solution(solve2, &input);
}
