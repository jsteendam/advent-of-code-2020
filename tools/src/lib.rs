use std::{fs, io::BufRead};
use std::{fs::File, io::BufReader, time::Instant};

pub fn time_and_print_solution<Argument, Result>(function: impl Fn(&Argument) -> Result, arg: &Argument)
where
    Result: std::fmt::Debug,
{
    let now = Instant::now();
    let solution = function(&arg);
    let duration = now.elapsed();
    println!(" Answer is {:?}, calulated in {:?}", solution, duration);
}

pub fn file_to_lines(filename: &str) -> Vec<String> {
    let file = File::open(filename).expect("Can't open file!");
    BufReader::new(file)
        .lines()
        .map(|line| line.unwrap())
        .collect()
}

pub fn file_to_string(filename: &str) -> String {
    fs::read_to_string(filename).expect("Can't open file!")
}
