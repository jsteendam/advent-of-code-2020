use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

struct Graph {
    edges: HashMap<String, Vec<(String, i32)>>,
}

impl Graph {
    fn new() -> Graph {
        Graph {
            edges: HashMap::new(),
        }
    }

    fn add_edge(&mut self, parent: String, child: String, child_count: i32) {
        let item = (child, child_count);
        self.edges
            .entry(parent)
            .and_modify(|entry| entry.push(item.clone()))
            .or_insert(vec![item]);
    }

    fn contains_bag(&self, parent: &str, bag: &str) -> bool {
        if parent == bag {
            return true;
        }
        if self.edges.contains_key(parent) {
            for edge in &self.edges[parent] {
                if self.contains_bag(&edge.0, bag) {
                    return true;
                }
            }
        }
        return false;
    }
    fn sum(&self, parent: &str) -> i32{
        let mut count = 0;
        if self.edges.contains_key(parent) {
            for edge in &self.edges[parent] {
                count += edge.1 + (edge.1 * self.sum(&edge.0));
            }
        }
        return count;
    }
}

fn parse_bag_string(s: &str) -> Result<(String, i32), ()> {
    let mut name = String::new();
    let mut count = 0;

    for token in s.split(" ") {
        if token == "bag" || token == "bags" {
            continue;
        }
        if token == "no" {
            return Err(());
        }
        let number = token.parse::<i32>();
        if number.is_ok() {
            count = number.unwrap();
        } else {
            if name.is_empty() {
                name.push_str(token);
            } else {
                name.push_str(" ");
                name.push_str(token);
            }
        }
    }

    Ok((name.trim().to_string(), count))
}

fn read_file(filename: &str) -> Graph {
    let file = File::open(filename).expect("Cannot open file!");
    let mut graph = Graph::new();
    for line_result in BufReader::new(file).lines() {
        let line = line_result.unwrap();
        let elements: Vec<&str> = line[..line.len() - 1].split("contain").collect();
        let parent = parse_bag_string(elements[0]).unwrap().0;
        for child_string in elements[1].split(", ") {
            let child_result = parse_bag_string(child_string);
            if child_result.is_ok() {
                let child = child_result.unwrap();
                graph.add_edge(parent.clone(), child.0, child.1);
            }
        }
    }
    return graph;
}

fn solve1(graph: &Graph) -> i32 {
    let mut count = 0;
    for item in &graph.edges {
        if item.0 == "shiny gold" {
            continue;
        }
        let found = graph.contains_bag(item.0, "shiny gold");
        if found {
            count += 1;
        }
    }
    return count;
}

fn solve2(graph: &Graph) -> i32 {
    return graph.sum("shiny gold");
}

fn run_tests() {
    let test_input1 = read_file("src/test_input1.txt");
    assert_eq!(solve1(&test_input1), 4);
    assert_eq!(solve2(&test_input1), 32);
    let test_input2 = read_file("src/test_input2.txt");
    assert_eq!(solve2(&test_input2), 126);
}

fn main() {
    run_tests();

    let input = read_file("src/input.txt");
    println!("How many bag colors can eventually contain at least one shiny gold bag?");
    println!("  {}", solve1(&input));
    println!("How many individual bags are required inside your single shiny gold bag?");
    println!("  {}", solve2(&input));
}
