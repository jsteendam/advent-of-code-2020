use crate::emulator::{Emulator, Program};
use itertools::Itertools;
use std::collections::HashMap;

mod emulator;

fn run_until_hang(emulator: &mut Emulator, program: &Program) {
    let mut execution_count = HashMap::new();
    emulator.load(program);
    loop {
        let entry = execution_count
            .entry(emulator.instruction_pointer)
            .and_modify(|value| *value += 1)
            .or_insert(1);
        if *entry == 2 {
            return;
        }
        emulator.step();

        if emulator.eof() {
            return;
        }
    }
}

fn solve1(program: &Program) -> i64 {
    let mut emulator = Emulator::new();
    run_until_hang(&mut emulator, program);
    return emulator.accumulator;
}

fn solve2(program: &Program) -> i64 {
    let mut emulator = Emulator::new();
    let indices =
        program
            .instructions
            .iter()
            .positions(|instruction| match instruction.opcode.as_str() {
                "jmp" => true,
                "nop" => true,
                _ => false,
            });
    for index in indices {
        let mut new_program = program.clone();
        let instruction = &mut new_program.instructions[index];
        instruction.opcode = match instruction.opcode.as_str() {
            "nop" => "jmp".to_string(),
            "jmp" => "nop".to_string(),
            _ => "".to_string(),
        };
        run_until_hang(&mut emulator, &new_program);

        if emulator.eof() {
            return emulator.accumulator;
        }
    }
    return -1;
}

#[cfg(test)]
mod tests {
    use crate::{emulator::Program, solve1, solve2};

    #[test]
    fn solution1() {
        let test_program = Program::from_file("src/test_input.txt");
        assert_eq!(solve1(&test_program), 5);
    }
    #[test]
    fn solution2() {
        let test_program = Program::from_file("src/test_input.txt");
        assert_eq!(solve2(&test_program), 8);
    }
}

fn main() {
    let program = Program::from_file("src/input.txt");
    println!("Run your copy of the boot code. Immediately before any instruction is executed a second time, what value is in the accumulator?");
    println!(" Accumulator value is {}", solve1(&program));
    println!("Fix the program so that it terminates normally by changing exactly one jmp (to nop) or nop (to jmp). What is the value of the accumulator after the program terminates?");
    println!(" Accumulator value is {}", solve2(&program));
}
