use std::num::ParseIntError;
use std::{
    fs::File,
    io::{BufRead, BufReader},
    str::FromStr,
};

#[derive(Clone)]
pub struct Instruction {
    pub opcode: String,
    pub argument: i32,
}

impl FromStr for Instruction {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let components: Vec<&str> = s.split(" ").collect();
        Ok(Instruction {
            opcode: components[0].to_string(),
            argument: components[1].parse().expect("Not an int!"),
        })
    }
}

#[derive(Clone)]
pub struct Program {
    pub name: String,
    pub instructions: Vec<Instruction>,
}

impl Program {
    pub fn from_file(filename: &str) -> Program {
        let file = File::open(filename).expect("Can't open file!");
        let reader = BufReader::new(file);
        Program {
            name: filename.to_string(),
            instructions: reader
                .lines()
                .map(|line| line.unwrap().parse().unwrap())
                .collect(),
        }
    }
}

pub struct Emulator {
    pub accumulator: i64,
    pub instruction_pointer: usize,
    pub program: Option<Program>,
}

impl Emulator {
    pub fn new() -> Emulator {
        Emulator {
            accumulator: 0,
            instruction_pointer: 0,
            program: None,
        }
    }

    pub fn load(&mut self, program: &Program) {
        self.accumulator = 0;
        self.instruction_pointer = 0;
        self.program = Some(program.clone());
    }

    pub fn step(&mut self) {
        match &self.program {
            Some(program) => {
                let instruction = {
                    if self.instruction_pointer >= program.instructions.len() {
                        return;
                    }
                    program.instructions[self.instruction_pointer].clone()
                };
                self.execute(&instruction);
            }
            None => {}
        };
    }

    pub fn execute(&mut self, instruction: &Instruction) {
        match instruction.opcode.as_str() {
            "acc" => self.accumulator += instruction.argument as i64,
            "jmp" => {
                if instruction.argument.is_negative() {
                    self.instruction_pointer -= instruction.argument.wrapping_abs() as usize
                } else {
                    self.instruction_pointer += instruction.argument.wrapping_abs() as usize
                }
                return;
            }

            "nop" => {}
            &_ => unreachable!(),
        }
        self.instruction_pointer += 1;
    }

    pub fn eof(&self) -> bool {
        match &self.program {
            Some(program) => self.instruction_pointer == program.instructions.len(),
            None => false,
        }
    }
}
