use std::fmt;
use std::fs::File;
use std::io::{BufRead, BufReader};

struct Map {
    data: Vec<Vec<bool>>,
}

impl Map {
    fn has_tree(&self, x: usize, y: usize) -> bool {
        let width = self.data[0].len();
        return self.data[y][x % width];
    }
    fn height(&self) -> usize {
        self.data.len()
    }
}

impl fmt::Display for Map {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut output = String::new();
        let height = &self.data.len();
        let mut row_number = 0;
        for row in &self.data {
            for square in row {
                if *square == true {
                    output += "#";
                } else {
                    output += ".";
                }
            }
            if row_number != (height - 1) {
                output += "\n";
            }
            row_number += 1;
        }
        write!(f, "{}", output)
    }
}

fn read_file(filename: &str) -> Map {
    let file = File::open(filename).expect("Cannot open file!");
    let reader = BufReader::new(file);
    let result = Map {
        data: reader
            .lines()
            .map(|line| line.unwrap().chars().map(|c| c == '#').collect())
            .collect(),
    };
    return result;
}

fn solve(map: &Map, step_x: usize, step_y: usize) -> i32 {
    let mut x: usize = 0;
    let mut y: usize = 0;
    let mut total_trees: i32 = 0;
    while y < map.height() {
        if map.has_tree(x, y) {
            total_trees += 1;
        }
        x += step_x;
        y += step_y;
    }
    return total_trees;
}

fn solve1(map: &Map) {
    println!("Solve1: {}", solve(&map, 3, 1));
}

fn solve2(map: &Map) {
    let slopes: Vec<(usize, usize)> = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let tree_count = slopes
        .iter()
        .map(|slope| solve(&map, slope.0, slope.1))
        .collect::<Vec<i32>>();
    println!(
        "Solve2: {}",
        tree_count.iter().fold(1, |acc, value| acc * value)
    );
    let it = slopes.iter().zip(tree_count.iter());
    for (_, (slope, count)) in it.enumerate() {
        println!("  Slope({}, {}): {}", slope.0, slope.1, count);
    }
}

fn main() {
    let test_input = read_file("src/test_input.txt");
    let input = read_file("src/input.txt");
    print!("Test ");
    solve1(&test_input);
    solve1(&input);
    print!("Test ");
    solve2(&test_input);
    solve2(&input);
}
