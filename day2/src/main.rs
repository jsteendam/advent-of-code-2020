use std::fs::File;
use std::io::{BufRead, BufReader};
use std::num::ParseIntError;
use std::str::FromStr;
#[macro_use]
extern crate lazy_static;
use regex::Regex;
use std::fmt;

struct PasswordPolicy {
    letter: char,
    min_occurence: usize,
    max_occurence: usize,
}

struct Record {
    policy: PasswordPolicy,
    password: String,
}

impl FromStr for PasswordPolicy {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref REGEX: Regex = Regex::new(r"^(\d+)-(\d+)\s(.)$").unwrap();
        }
        assert!(REGEX.is_match(s));
        let capture = REGEX.captures_iter(s).next().unwrap();
        Ok(PasswordPolicy {
            min_occurence: capture[1].parse().unwrap(),
            max_occurence: capture[2].parse().unwrap(),
            letter: capture[3].parse().unwrap(),
        })
    }
}

impl FromStr for Record {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref REGEX: Regex = Regex::new(r"^(.+):\s(.+)$").unwrap();
        }
        assert!(REGEX.is_match(s));
        let capture = REGEX.captures_iter(s).next().unwrap();
        Ok(Record {
            policy: capture[1].parse().unwrap(),
            password: capture[2].to_string(),
        })
    }
}

impl fmt::Display for PasswordPolicy {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}-{} {}",
            self.min_occurence, self.max_occurence, self.letter
        )
    }
}

impl fmt::Display for Record {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}: {}", self.policy, self.password)
    }
}

fn read_file(filename: &str) -> Vec<Record> {
    let file = File::open(filename).expect("Cannot open file!");
    let reader = BufReader::new(file);
    let result: Vec<Record> = reader
        .lines()
        .map(|line| line.unwrap().parse().expect("Invalid line in file"))
        .collect();
    return result;
}

fn read_string(s: &str) -> Vec<Record> {
    let result: Vec<Record> = s
        .lines()
        .map(|line| line.parse().expect("Invalid line in file"))
        .collect();
    return result;
}

fn solve1(records: &Vec<Record>) {
    let mut result = 0;
    for record in records {
        let count = record.password.matches(record.policy.letter).count();
        if count >= record.policy.min_occurence && count <= record.policy.max_occurence {
            result += 1;
        }
    }
    println!("Solve1 {}", result);
}

fn solve2(records: &Vec<Record>) {
    let mut result = 0;
    for record in records {
        let pos1 = record.policy.min_occurence;
        let pos2 = record.policy.max_occurence;
        let char_pos1 = record.password.chars().nth(pos1 - 1).unwrap();
        let char_pos2 = record.password.chars().nth(pos2 - 1).unwrap();
        let letter = record.policy.letter;
        if (char_pos1 == letter) ^ (char_pos2 == letter) {
            result += 1;
        }
    }
    println!("Solve2 {}", result);
}

fn main() {
    let test_input = read_string("1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc\n");
    let input = read_file("src/input.txt");
    print!("Test ");
    solve1(&test_input);
    solve1(&input);
    print!("Test ");
    solve2(&test_input);
    solve2(&input);
}
