use std::{collections::HashMap, convert::TryInto};

fn solve(input: &String, target: usize) -> i64 {
    let starting_numbers: Vec<i64> = input
        .split(",")
        .map(|element| element.parse().unwrap())
        .collect();
    let mut history = HashMap::new();
    let mut round_number = 0;
    let mut last_number = 0;
    loop {
        if round_number < starting_numbers.len() {
            history.insert(starting_numbers[round_number], vec![round_number]);
            last_number = starting_numbers[round_number];
        } else if history.contains_key(&last_number) {
            let number = if history[&last_number].len() == 1 {
                0
            } else {
                let size = history[&last_number].len();
                history[&last_number][size - 1] - history[&last_number][size - 2]
            };

            history
                .entry(number.try_into().unwrap())
                .and_modify(|entry| entry.push(round_number))
                .or_insert(vec![round_number]);

            last_number = number.try_into().unwrap();
        } else {
            unreachable!();
        }

        round_number += 1;

        if round_number >= target {
            break;
        }
    }
    return last_number;
}

fn solve1(input: &String) -> i64 {
    solve(input, 2020)
}

fn solve2(input: &String) -> i64 {
    solve(input, 30000000)
}

#[cfg(test)]
mod test {
    use crate::solve1;

    #[test]
    fn solution1() {
        assert_eq!(solve1(&"0,3,6".to_string()), 436);
        assert_eq!(solve1(&"1,3,2".to_string()), 1);
        assert_eq!(solve1(&"2,1,3".to_string()), 10);
        assert_eq!(solve1(&"1,2,3".to_string()), 27);
        assert_eq!(solve1(&"2,3,1".to_string()), 78);
        assert_eq!(solve1(&"3,2,1".to_string()), 438);
        assert_eq!(solve1(&"3,1,2".to_string()), 1836);
    }

    #[test]
    fn solution2() {
        assert_eq!(solve1(&"0,3,6".to_string()), 175594);
        assert_eq!(solve1(&"1,3,2".to_string()), 2578);
        assert_eq!(solve1(&"2,1,3".to_string()), 3544142);
        assert_eq!(solve1(&"1,2,3".to_string()), 261214);
        assert_eq!(solve1(&"2,3,1".to_string()), 6895259);
        assert_eq!(solve1(&"3,2,1".to_string()), 18);
        assert_eq!(solve1(&"3,1,2".to_string()), 362);
    }
}

fn main() {
    println!("Given your starting numbers, what will be the 2020th number spoken?");
    tools::time_and_print_solution(solve1, &"0,14,6,20,1,4".to_string());
    println!("Given your starting numbers, what will be the 30000000th number spoken?");
    tools::time_and_print_solution(solve2, &"0,14,6,20,1,4".to_string());
}
