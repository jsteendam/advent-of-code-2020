use std::fs::File;
use std::io::{BufRead, BufReader};
use std::num::ParseIntError;
use std::str::FromStr;

struct BoardingPass {
    row: i32,
    column: i32,
    seat_id: i32,
}

impl FromStr for BoardingPass {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let row = Self::do_partitioning(s[..7].chars(), (0, 127));
        let column = Self::do_partitioning(s[7..].chars(), (0, 7));
        Ok(BoardingPass {
            row: row.0,
            column: column.1,
            seat_id: (row.0 * 8) + column.1,
        })
    }
}

impl BoardingPass {
    fn do_partitioning(chars: std::str::Chars, mut range: (i32, i32)) -> (i32, i32) {
        for c in chars {
            if c == 'F' || c == 'L' {
                range.1 -= ((range.1 - range.0) as f32 / 2_f32).round() as i32;
            } else if c == 'B' || c == 'R' {
                range.0 += ((range.1 - range.0) as f32 / 2_f32).round() as i32;
            }
        }
        return range;
    }
}

fn read_file(filename: &str) -> Vec<BoardingPass> {
    let file = File::open(filename).expect("Cannot open file!");
    BufReader::new(file)
        .lines()
        .map(|line| line.unwrap().parse().unwrap())
        .collect()
}

fn run_tests() {
    let test_input_1 = "FBFBBFFRLR".parse::<BoardingPass>().unwrap();
    let test_input_2 = "BFFFBBFRRR".parse::<BoardingPass>().unwrap();
    let test_input_3 = "FFFBBBFRRR".parse::<BoardingPass>().unwrap();
    let test_input_4 = "BBFFBBFRLL".parse::<BoardingPass>().unwrap();
    assert!(test_input_1.row == 44 && test_input_1.column == 5 && test_input_1.seat_id == 357);
    assert!(test_input_2.row == 70 && test_input_2.column == 7 && test_input_2.seat_id == 567);
    assert!(test_input_3.row == 14 && test_input_3.column == 7 && test_input_3.seat_id == 119);
    assert!(test_input_4.row == 102 && test_input_4.column == 4 && test_input_4.seat_id == 820);
}

fn solve1(passes: &Vec<BoardingPass>) -> i32 {
    passes.iter().map(|pass| pass.seat_id).max().unwrap()
}

fn solve2(passes: &Vec<BoardingPass>) -> i32 {
    let mut seats: Vec<i32> = passes.iter().map(|pass| pass.seat_id).collect();
    seats.sort();
    for (id1, id2) in seats.iter().zip(seats.iter().skip(1)) {
        if id1 + 2 == *id2 {
            return id1 + 1;
        }
    }
    return -1;
}

fn main() {
    run_tests();

    let input = read_file("src/input.txt");
    println!("What is the highest seat ID on a boarding pass?");
    println!("  Highest seat ID is {}", solve1(&input));
    println!("What is the ID of your seat?");
    println!("  My seat ID is {}", solve2(&input));
}
