#[macro_use]
extern crate lazy_static;

mod day16;
fn main() {
    day16::solve();
}
