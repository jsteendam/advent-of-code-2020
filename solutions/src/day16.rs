use regex::Regex;
use std::io::BufRead;

trait Rule {
    fn validate(&self, input: i64) -> bool;
}

struct RangeRule {
    name: String,
    min: i64,
    max: i64,
}

impl Rule for RangeRule {
    fn validate(&self, input: i64) -> bool {
        input >= self.min && input<= self.max
    }
}

enum NotesParserState {
    Rules,
    YourTicket,
    NearbyTickets,
}

struct Notes {
    rules: Vec<RangeRule>,
    your_ticket: String,
    nearby_tickets: Vec<String>,
}

impl Notes {
    fn from_file(filename: &str) -> Self {
        let lines = tools::file_to_lines(filename);
        let mut state = NotesParserState::Rules;
        let mut rules = Vec::new();
        let mut your_ticket = String::new();
        let mut nearby_tickets = Vec::new();
        for line in lines {
            if line.trim().is_empty() {
                state = match state {
                    NotesParserState::Rules => NotesParserState::YourTicket,
                    NotesParserState::YourTicket => NotesParserState::NearbyTickets,
                    NotesParserState::NearbyTickets => unreachable!(),
                };
                continue;
            }
            match state {
                NotesParserState::Rules => {
                    lazy_static! {
                        static ref REGEX: Regex =
                            Regex::new(r"^(.+): (\d+)-(\d+) or (\d+)-(\d+)$").unwrap();
                    }
                    if !REGEX.is_match(line.as_str()) {
                        continue;
                    }
                    let capture = REGEX.captures_iter(line.as_str()).next().unwrap();
                    rules.push(RangeRule {
                        name: capture[1].parse().unwrap(),
                        min: capture[2].parse().unwrap(),
                        max: capture[3].parse().unwrap(),
                    });
                    rules.push(RangeRule {
                        name: capture[1].parse().unwrap(),
                        min: capture[4].parse().unwrap(),
                        max: capture[5].parse().unwrap(),
                    });
                }
                NotesParserState::YourTicket => {
                    if !line.contains(":") {
                        your_ticket = line;
                    }
                }
                NotesParserState::NearbyTickets => {
                    if !line.contains(":") {
                        nearby_tickets.push(line);
                    }
                }
            };
            // println!("{}", line);
        }
        Self {
            rules: rules,
            your_ticket: your_ticket,
            nearby_tickets: nearby_tickets,
        }
    }
}

fn solve1(notes: &Notes) -> i64 {
    let mut sum = 0;
    for ticket in &notes.nearby_tickets {
        let numbers: Vec<i64> = ticket
            .split(",")
            .map(|value| value.parse().unwrap())
            .collect();
        for number in numbers {
            let mut valid = false;
            for rule in &notes.rules {
                if rule.validate(number) {
                    valid = true;
                    break;
                }
            }
            if !valid {
                sum += number;
            }
        }
    }
    return sum;
}

fn solve2(notes: &Notes) -> i64 {
    let mut sum = 0;
    let mut tickets = notes.nearby_tickets.clone();
    tickets.retain(|item| {
        let numbers: Vec<i64> = item
            .split(",")
            .map(|value| value.parse().unwrap())
            .collect();
        for number in numbers {
            let mut valid = false;
            for rule in &notes.rules {
                if rule.validate(number) {
                    valid = true;
                    break;
                }
            }
            if !valid {
                return false;
            }
        }
        return true;
    });
    println!("{:?}", tickets);

    return sum;
}

pub fn solve() {
    let input = Notes::from_file(r"solutions\input\day16\input.txt");
    let test_input = Notes::from_file(r"solutions\input\day16\test_input2.txt");
    println!("Day 16");
    // println!("What is your ticket scanning error rate?");
    // tools::time_and_print_solution(solve1, &input);
    println!("What is your ticket scanning error rate?");
    tools::time_and_print_solution(solve2, &test_input);
}
