use tools::time_and_print_solution;
use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

struct Graph<T> {
    edges: HashMap<T, Vec<(T, i32)>>,
}

impl<T> Graph<T>
where
    T: Clone + Eq + std::hash::Hash + PartialOrd,
{
    fn new() -> Graph<T> {
        Graph {
            edges: HashMap::new(),
        }
    }

    fn len(&self) -> usize {
        self.edges.len()
    }

    fn add_node(&mut self, value: &T) {
        self.edges.entry(value.clone()).or_insert(Vec::new());
    }

    fn add_edge(&mut self, parent: &T, child: &T, weight: i32) {
        self.edges
            .entry(parent.clone())
            .and_modify(|entry| entry.push((child.clone(), weight.clone())))
            .or_insert(vec![(child.clone(), weight.clone())]);
    }

    fn get_longest_path(&self, start: &T, end: &T) -> Vec<T> {
        let mut result = vec![start.clone()];
        if start == end {
            return result;
        }
        if self.edges.contains_key(start) {
            let mut lowest_weight = i32::MAX;
            let mut lowest_weight_edge = Option::None;
            for edge in &self.edges[start] {
                if edge.1 < lowest_weight {
                    lowest_weight = edge.1;
                    lowest_weight_edge = Some(edge);
                }
            }
            match lowest_weight_edge {
                Some(edge) => {
                    let mut path = self.get_longest_path(&edge.0, end);
                    result.append(&mut path);
                    return result;
                }
                None => {}
            }
        }
        return result;
    }

    fn get_path_count(&self, start: &T, end: &T, cache: &mut HashMap<T, usize>) -> usize {
        let mut result = 0;
        if start == end {
            cache.insert(start.clone(), result);
            return result;
        }
        if cache.contains_key(start) {
            return cache[start];
        }
        if self.edges.contains_key(start) {
            let edges = &self.edges[start];
            result += edges.len() - 1;
            for edge in edges {
                let count = self.get_path_count(&edge.0, end, cache);
                result += count;
            }
        }
        cache.insert(start.clone(), result);
        return result;
    }
}

fn read_file(filename: &str) -> Graph<i32> {
    let file = File::open(filename).expect("Can't open file!");
    let mut graph = Graph::new();
    let mut adapters: Vec<i32> = BufReader::new(file)
        .lines()
        .map(|line| line.unwrap().parse().unwrap())
        .collect();
    let max = match adapters.iter().max() {
        Some(value) => value.clone(),
        None => unreachable!(),
    };
    adapters.push(0);
    adapters.push(max + 3);
    for adapter1 in &adapters {
        graph.add_node(adapter1);
        for adapter2 in &adapters {
            let diff = adapter2 - adapter1;
            if diff > 0 && diff <= 3 {
                graph.add_edge(adapter1, adapter2, diff);
            }
        }
    }
    return graph;
}

fn solve1(graph: &Graph<i32>) -> usize {
    let max = graph
        .edges
        .iter()
        .map(|entry| entry.0.clone())
        .max()
        .unwrap();
    let path = &graph.get_longest_path(&0, &max);
    assert_eq!(graph.len(), path.len());
    let differences: Vec<i32> = path.windows(2).map(|items| items[1] - items[0]).collect();
    let number_of_ones = differences.iter().filter(|value| **value == 1).count();
    let number_of_threes = differences.iter().filter(|value| **value == 3).count();
    return number_of_ones * number_of_threes;
}

fn solve2(graph: &Graph<i32>) -> usize {
    let max = graph
        .edges
        .iter()
        .map(|entry| entry.0.clone())
        .max()
        .unwrap();
    let mut cache = HashMap::new();
    return graph.get_path_count(&0, &max, &mut cache) + 1;
}

#[cfg(test)]
mod tests {
    use crate::read_file;
    use crate::solve1;
    use crate::solve2;

    #[test]
    fn solution1() {
        let test_input1 = read_file("day10/src/test_input1.txt");
        let test_input2 = read_file("day10/src/test_input2.txt");
        assert_eq!(solve1(&test_input1), 7 * 5);
        assert_eq!(solve1(&test_input2), 22 * 10);
    }

    #[test]
    fn solution2() {
        let test_input1 = read_file("day10/src/test_input1.txt");
        let test_input2 = read_file("day10/src/test_input2.txt");
        assert_eq!(solve2(&test_input1), 8);
        assert_eq!(solve2(&test_input2), 19208);
    }
}

fn main() {
    let input = read_file("day10/src/input.txt");
    println!(
        "What is the number of 1-jolt differences multiplied by the number of 3-jolt differences?"
    );
    time_and_print_solution(solve1, &input);
    println!(
        "What is the total number of distinct ways you can arrange the adapters to connect the charging outlet to your device?"
    );
    time_and_print_solution(solve2, &input);
}
