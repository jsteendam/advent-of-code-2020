use itertools::{
    Itertools,
    MinMaxResult::{MinMax, NoElements, OneElement},
};
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn read_file(filename: &str) -> Vec<i64> {
    let file = File::open(filename).expect("Can't open file");
    BufReader::new(file)
        .lines()
        .map(|line| line.unwrap().parse().unwrap())
        .collect()
}

fn solve1(numbers: &Vec<i64>, preamble: i32) -> i64 {
    let start = preamble as usize;
    let end = numbers.len();
    for n in start..end {
        let mut found = false;
        for i in n - preamble as usize..n {
            for j in n - preamble as usize..n {
                if i != j {
                    if numbers[n] == (numbers[i] + numbers[j]) {
                        found = true;
                        break;
                    }
                }
            }
            if found {
                break;
            }
        }
        if !found {
            return numbers[n];
        }
    }
    return -1;
}

fn solve2(numbers: &Vec<i64>, preamble: i32) -> i64 {
    let invalid_number = solve1(&numbers, preamble);
    for n in 0..numbers.len() {
        let mut current_sum = 0;
        let subset: Vec<&i64> = numbers
            .iter()
            .skip(n)
            .take_while(|value| {
                current_sum += **value;
                current_sum < invalid_number
            })
            .collect();
        if current_sum == invalid_number {
            match subset.iter().minmax() {
                NoElements => continue,
                OneElement(_) => continue,
                MinMax(min, max) => return *min + *max,
            };
        }
    }
    return -1;
}

#[cfg(test)]
mod tests {
    use crate::read_file;
    use crate::solve1;
    use crate::solve2;
    #[test]
    fn solution1() {
        let test_input = read_file("src/test_input.txt");
        assert_eq!(solve1(&test_input, 5), 127);
    }
    #[test]
    fn solution2() {
        let test_input = read_file("src/test_input.txt");
        assert_eq!(solve2(&test_input, 5), 62);
    }
}

fn main() {
    let input = read_file("day9/src/input.txt");
    println!("The first step of attacking the weakness in the XMAS data is to find the first number in the list (after the preamble) which is not the sum of two of the 25 numbers before it. What is the first number that does not have this property?");
    println!("  First number is {}", solve1(&input, 25));
    println!("What is the encryption weakness in your XMAS-encrypted list of numbers?");
    println!("  Weakness is {}", solve2(&input, 25));
}
