use std::fs;

fn solve1(input: &Vec<i32>) {
    println!("Solve1:");
    for value1 in input.iter() {
        for value2 in input.iter() {
            if value1 == value2 {
                continue;
            }
            let sum = value1 + value2;
            if sum == 2020 {
                let product = value1 * value2;
                println!("{} * {} = {}", value1, value2, product);
            }
        }
    }
}
fn solve2(input: &Vec<i32>) {
    println!("Solve2:");
    for value1 in input.iter() {
        for value2 in input.iter() {
            for value3 in input.iter() {
                if value1 == value2 || value1 == value3 || value2 == value3 {
                    continue;
                }
                let sum = value1 + value2 + value3;
                if sum == 2020 {
                    let product = value1 * value2 * value3;
                    println!("{} * {} * {} = {}", value1, value2, value3, product);
                }
            }
        }
    }
}

fn read_file_to_lines(filename: &str) -> Vec<i32> {
    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");
    let lines: Vec<i32> = contents
        .lines()
        .map(|s| s.parse().expect("parse error"))
        .collect();
    return lines;
}

fn main() {
    let test_input: Vec<i32> = vec![1721, 979, 366, 299, 675, 1456];
    print!("Test ");
    solve1(&test_input);
    print!("Test ");
    solve2(&test_input);
    let input = read_file_to_lines("src/input.txt");
    solve1(&input);
    solve2(&input);
}
