use std::io::BufRead;
use std::{fs::File, io::BufReader, str::FromStr};

enum InstructionType {
    North,
    East,
    South,
    West,
    Left,
    Right,
    Forward,
}

impl FromStr for InstructionType {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "N" => InstructionType::North,
            "E" => InstructionType::East,
            "S" => InstructionType::South,
            "W" => InstructionType::West,
            "L" => InstructionType::Left,
            "R" => InstructionType::Right,
            "F" => InstructionType::Forward,
            _ => unreachable!(),
        })
    }
}

enum Orientation {
    North = 0,
    East,
    South,
    West,
}

struct Instruction {
    instruction_type: InstructionType,
    value: i64,
}

impl FromStr for Instruction {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Instruction {
            instruction_type: s[0..1].parse().unwrap(),
            value: s[1..].parse().unwrap(),
        })
    }
}

struct Waypoint {
    x: i64,
    y: i64,
}

struct Boat {
    orientation: Orientation,
    x: i64,
    y: i64,
    waypoint: Waypoint,
}

impl Boat {
    fn turn_left(&mut self, mut value: i64) {
        while value >= 90 {
            self.orientation = match self.orientation {
                Orientation::North => Orientation::West,
                Orientation::East => Orientation::North,
                Orientation::South => Orientation::East,
                Orientation::West => Orientation::South,
            };
            value -= 90;
        }
    }
    fn turn_right(&mut self, mut value: i64) {
        while value >= 90 {
            self.orientation = match self.orientation {
                Orientation::North => Orientation::East,
                Orientation::East => Orientation::South,
                Orientation::South => Orientation::West,
                Orientation::West => Orientation::North,
            };
            value -= 90;
        }
    }
    fn forward(&mut self, value: i64) {
        match self.orientation {
            Orientation::North => self.y -= value,
            Orientation::East => self.x += value,
            Orientation::South => self.y += value,
            Orientation::West => self.x -= value,
        };
    }
}

fn read_file(filename: &str) -> Vec<Instruction> {
    let file = File::open(filename).expect("Can't open file!");
    BufReader::new(file)
        .lines()
        .map(|line| line.unwrap().parse().unwrap())
        .collect()
}

fn solve1(instructions: &Vec<Instruction>) -> i64 {
    let mut boat = Boat {
        orientation: Orientation::East,
        x: 0,
        y: 0,
        waypoint: Waypoint { x: 0, y: 0 },
    };
    for instruction in instructions {
        match instruction.instruction_type {
            InstructionType::North => boat.y -= instruction.value,
            InstructionType::East => boat.x += instruction.value,
            InstructionType::South => boat.y += instruction.value,
            InstructionType::West => boat.x -= instruction.value,
            InstructionType::Left => boat.turn_left(instruction.value),
            InstructionType::Right => boat.turn_right(instruction.value),
            InstructionType::Forward => boat.forward(instruction.value),
        };
    }
    return i64::abs(boat.x) + i64::abs(boat.y);
}

fn solve2(instructions: &Vec<Instruction>) -> i64 {
    let mut boat = Boat {
        orientation: Orientation::East,
        x: 0,
        y: 0,
        waypoint: Waypoint { x: 10, y: -1 },
    };
    for instruction in instructions {
        match instruction.instruction_type {
            InstructionType::North => boat.waypoint.y -= instruction.value,
            InstructionType::East => boat.waypoint.x += instruction.value,
            InstructionType::South => boat.waypoint.y += instruction.value,
            InstructionType::West => boat.waypoint.x -= instruction.value,
            InstructionType::Left => {
                for _ in (0..instruction.value).step_by(90) {
                    let tmp = boat.waypoint.x;
                    boat.waypoint.x = boat.waypoint.y;
                    boat.waypoint.y = -tmp;
                }
            }
            InstructionType::Right => {
                for _ in (0..instruction.value).step_by(90) {
                    let tmp = boat.waypoint.x;
                    boat.waypoint.x = -boat.waypoint.y;
                    boat.waypoint.y = tmp;
                }
            }
            InstructionType::Forward => {
                boat.x += boat.waypoint.x * instruction.value;
                boat.y += boat.waypoint.y * instruction.value;
            }
        };
    }
    return i64::abs(boat.x) + i64::abs(boat.y);
}

#[cfg(test)]
mod tests {
    use crate::{read_file, solve1, solve2};
    #[test]
    fn solution1() {
        let test_input = read_file("day12/src/test_input.txt");
        assert_eq!(solve1(&test_input), 25);
    }

    #[test]
    fn solution2() {
        let test_input = read_file("day12/src/test_input.txt");
        assert_eq!(solve2(&test_input), 286);
    }
}

fn main() {
    let input = read_file("day12/src/input.txt");
    println!(
        "What is the Manhattan distance between that location and the ship's starting position?"
    );
    tools::time_and_print_solution(solve1, &input);
    println!(
        "What is the Manhattan distance between that location and the ship's starting position?"
    );
    tools::time_and_print_solution(solve2, &input);
}
