use std::collections::HashMap;
use std::io::{BufRead, BufReader};
use std::{collections::HashSet, fs::File};

struct Group {
    persons: Vec<String>,
}

fn read_file(filename: &str) -> Vec<Group> {
    let file = File::open(filename).expect("Cannot open file!");
    let lines: Vec<String> = BufReader::new(file)
        .lines()
        .map(|line| line.unwrap())
        .collect();
    let groups: Vec<Group> = lines
        .split(|line| line == "")
        .map(|items| Group {
            persons: items.to_vec(),
        })
        .collect();
    return groups;
}

fn solve1(groups: &Vec<Group>) -> usize {
    let mut count = 0;
    for group in groups {
        let result = group
            .persons
            .iter()
            .fold("".to_owned(), |acc, person| acc + person);
        let mut set = HashSet::new();
        result.chars().for_each(|c| {
            set.insert(c);
        });
        count += set.len();
    }
    return count;
}

fn solve2(groups: &Vec<Group>) -> usize {
    let mut count = 0;
    for group in groups {
        let mut map: HashMap<char, usize> = HashMap::new();
        let person_count = group.persons.len();
        for person in &group.persons {
            for c in person.chars() {
                map.entry(c).and_modify(|n| *n += 1).or_insert(1);
            }
        }
        count += map.iter().filter(|(_key,value)| **value == person_count).count();
    }
    return count;
}

fn run_tests() {
    let test_input = read_file("src/test_input.txt");
    assert_eq!(solve1(&test_input), 11);
    assert_eq!(solve2(&test_input), 6);
}

fn main() {
    run_tests();

    let input = read_file("src/input.txt");
    println!("For each group, count the number of questions to which anyone answered \"yes\". What is the sum of those counts?");
    println!("  Sum is {}", solve1(&input));
    println!("For each group, count the number of questions to which everyone answered \"yes\". What is the sum of those counts?");
    println!("  Sum is {}", solve2(&input));
}
