use std::collections::HashMap;

enum ParserState {
    Command,
    MemoryLocation,
    Separator,
    Value,
}

struct Parser {
    state: ParserState,
}

impl Parser {
    fn new() -> Self {
        Self {
            state: ParserState::Command,
        }
    }

    fn parse_line(&mut self, line: &str) -> (String, i64, String) {
        let mut command = String::new();
        let mut memory_location = String::new();
        let mut value = String::new();
        let mut i = 0;
        let char_count = line.chars().count();
        while i < char_count {
            let c = line.chars().nth(i).unwrap();
            self.state = match self.state {
                ParserState::Command => match c {
                    ' ' => ParserState::Separator,
                    '[' => ParserState::MemoryLocation,
                    _ => {
                        command.push(c);
                        i += 1;
                        ParserState::Command
                    }
                },
                ParserState::Separator => match c {
                    ' ' => {
                        i += 1;
                        ParserState::Separator
                    }
                    '=' => {
                        i += 1;
                        ParserState::Separator
                    }
                    _ => ParserState::Value,
                },
                ParserState::MemoryLocation => match c {
                    '[' => {
                        i += 1;
                        ParserState::MemoryLocation
                    }
                    ']' => {
                        i += 1;
                        ParserState::Separator
                    }
                    _ => {
                        memory_location.push(c);
                        i += 1;
                        ParserState::MemoryLocation
                    }
                },
                ParserState::Value => {
                    value.push(c);
                    i += 1;
                    ParserState::Value
                }
            };
        }
        self.state = ParserState::Command;
        return (command, memory_location.parse().unwrap_or(0), value);
    }
}

struct Memory {
    map: HashMap<i64, i64>,
    or_mask: i64,
    and_mask: i64,
}

impl Memory {
    fn new() -> Self {
        Self {
            map: HashMap::new(),
            or_mask: 0,
            and_mask: i64::MAX,
        }
    }

    fn set_mask(&mut self, mask: &String) {
        self.or_mask = 0;
        self.and_mask = 0;
        for c in mask.chars() {
            self.or_mask <<= 1;
            self.and_mask <<= 1;

            match c {
                'X' => self.and_mask |= 1,
                '1' => self.or_mask |= 1,
                '0' => {}
                _ => unreachable!(),
            }
        }
    }

    fn write(&mut self, location: i64, mut data: i64) {
        data &= self.and_mask;
        data |= self.or_mask;
        self.map
            .entry(location)
            .and_modify(|value| *value = data)
            .or_insert(data);
    }
}

fn solve1(lines: &Vec<String>) -> i64 {
    let mut parser = Parser::new();
    let mut memory = Memory::new();
    for line in lines {
        let result = parser.parse_line(&line[0..]);
        match result.0.as_str() {
            "mask" => memory.set_mask(&result.2),
            "mem" => memory.write(result.1, result.2.parse().unwrap()),
            _ => unreachable!(),
        };
    }
    memory.map.iter().map(|entry| *entry.1).sum()
}

fn solve2(lines: &Vec<String>) -> i64 {
    let mut parser = Parser::new();
    let mut memory = Memory::new();

    let mut mask = 0;
    let mut floating_indices = Vec::new();

    for line in lines {
        let result = parser.parse_line(&line[0..]);
        match result.0.as_str() {
            "mask" => {
                let mut index = 35; // MSB index
                mask = 0;
                floating_indices.clear();
                for c in result.2.chars() {
                    mask <<= 1;
                    match c {
                        'X' => floating_indices.push(index),
                        '1' => mask |= 1,
                        _ => {}
                    };
                    index -= 1;
                }
            }
            "mem" => {
                let memory_location = result.1 | mask;
                for n in 0..(1 << floating_indices.len()) {
                    let mut count = floating_indices.len();
                    let mut location = memory_location;
                    for index in &floating_indices {
                        count -= 1;
                        let value = ((n >> count) & 1) as i64;
                        if value == 1 {
                            location |= value << index;
                        } else {
                            location &= !(1 << index);
                        }
                    }
                    memory.write(location, result.2.parse().unwrap())
                }
            }
            _ => unreachable!(),
        };
    }
    memory.map.iter().map(|entry| *entry.1).sum()
}

#[cfg(test)]
mod tests {
    use crate::solve1;
    use crate::solve2;

    #[test]
    fn solution1() {
        let lines = tools::file_to_lines("day14/src/test_input1.txt");
        assert_eq!(solve1(&lines), 165);
    }

    #[test]
    fn solution2() {
        let lines = tools::file_to_lines("day14/src/test_input2.txt");
        assert_eq!(solve2(&lines), 208);
    }
}

fn main() {
    let lines = tools::file_to_lines("day14/src/input.txt");
    println!("What is the sum of all values left in memory after it completes?");
    tools::time_and_print_solution(solve1, &lines);
    println!("What is the sum of all values left in memory after it completes?");
    tools::time_and_print_solution(solve2, &lines);
}
