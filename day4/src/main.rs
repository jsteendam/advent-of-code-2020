use std::fs;
use std::collections::HashMap;
use std::num::ParseIntError;
use std::str::FromStr;
use std::i32;

struct Passport {
    fields: HashMap<String, String>,
}

impl Passport {
    fn field_count(&self) -> usize {
        self.fields.len()
    }
    fn has_field(&self, name: &str) -> bool {
        self.fields.contains_key(name)
    }
    fn is_valid(&self) -> bool {
        if self.field_count() == 8 {
            return true;
        }
        if self.field_count() == 7 && !self.has_field("cid") {
            return true;
        }
        return false;
    }
    fn is_valid_strict(&self) -> bool {
        if self.is_valid() {
            let result = self.is_byr_valid()
                && self.is_iyr_valid()
                && self.is_eyr_valid()
                && self.is_hgt_valid()
                && self.is_hcl_valid()
                && self.is_ecl_valid()
                && self.is_pid_valid();
            return result;
        }
        return false;
    }
    fn is_byr_valid(&self) -> bool {
        return self.validate_number_range("byr", (1920, 2002));
    }
    fn is_iyr_valid(&self) -> bool {
        return self.validate_number_range("iyr", (2010, 2020));
    }
    fn is_eyr_valid(&self) -> bool {
        return self.validate_number_range("eyr", (2020, 2030));
    }
    fn is_hgt_valid(&self) -> bool {
        if self.has_field("hgt") {
            let field = self.fields["hgt"].to_string();
            let field_unit = &field[field.len()-2..];
            let field_number = &field[..field.len()-2];
            if field_unit == "cm" {
                let value = field_number.to_string().parse::<i32>().expect("not an int!");
                return value >= 150 && value <= 193;
            } else if field_unit == "in" {
                let value = field_number.to_string().parse::<i32>().expect("not an int!");
                return value >= 59 && value <= 76;
            }
        }
        return false;
    }
    fn is_hcl_valid(&self) -> bool {
        if self.has_field("hcl") {
            let field = self.fields["hcl"][1..].to_string();
            let value = i32::from_str_radix(&field, 16);
            return field.char_indices().count() == 6 && value.is_ok() && value.unwrap() > 0;
        }
        return false;
    }
    fn is_ecl_valid(&self) -> bool {
        let colors = vec!["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
        if self.has_field("ecl") {
            return colors.iter().any(|s| self.fields["ecl"] == s.to_string());
        }
        return false;
    }
    fn is_pid_valid(&self) -> bool {
        if self.has_field("pid") {
            let field = self.fields["pid"].to_string();
            let number = field.parse::<i32>();
            return field.chars().count() == 9 && number.is_ok();
        }
        return false;
    }
    fn validate_number_range(&self, field: &str, range: (i32, i32)) -> bool {
        if self.has_field(field) {
            let value = self.fields[field]
                .to_string()
                .parse::<i32>()
                .expect("not an int!");
            return value >= range.0 && value <= range.1;
        }
        return false;
    }
}

impl FromStr for Passport {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut fields: HashMap<String, String> = HashMap::new();
        for field in s.split_whitespace() {
            let mut elements = field.split(":").map(|s| s.to_string());
            fields.insert(elements.next().unwrap(), elements.next().unwrap());
        }
        Ok(Passport { fields: fields })
    }
}

fn read_file(filename: &str) -> Vec<Passport> {
    let content = fs::read_to_string(filename).expect("Cannot open file!");
    let mut result: Vec<Passport> = vec![];
    for item in content.split("\n\n") {
        let s = item.to_string();
        result.push(s.parse().unwrap());
    }
    return result;
}

fn solve1(passports: &Vec<Passport>) {
    println!(
        "Solve1 {}",
        passports
            .iter()
            .filter(|&passport| passport.is_valid())
            .count()
    );
}

fn solve2(passports: &Vec<Passport>) {
    println!(
        "Solve2 {}",
        passports
            .iter()
            .filter(|&passport| passport.is_valid_strict())
            .count()
    );
}

fn main() {
    let test_input = read_file("src/test_input.txt");
    let invalid_passports = read_file("src/invalid_passports.txt");
    let valid_passports = read_file("src/valid_passports.txt");
    let input = read_file("src/input.txt");

    print!("Test ");
    solve1(&test_input);
    solve1(&input);
    
    print!("Test ");
    solve2(&test_input);
    print!("Invalid passwords ");
    solve2(&invalid_passports);
    print!("Valid passwords ");
    solve2(&valid_passports);
    solve2(&input);
}
